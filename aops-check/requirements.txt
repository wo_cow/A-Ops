Flask==2.0.2
Flask_RESTful==0.3.9
marshmallow==3.14.1
numpy==1.21.5
pandas==1.3.5
prometheus_api_client==0.5.0
setuptools==40.8.0
